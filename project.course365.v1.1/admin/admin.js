const gDATA = ['id', "courseCode", 'courseName', 'price', 'discountPrice', 'duration', 'level', 'coverImage', 'teacherName', 'isPopular', 'isTrending', 'action']
const gID_COL = 0;
const gCOURSE_CODE_COL = 1;
const gCOURSE_NAME_COL = 2;
const gPRICE_COL = 3;
const gDISCOUNT_PRICE_COL = 4;
const gDURATION_COL = 5;
const gLEVEL_COL = 6;
const gCOVER_IMAGE_COL = 7;
const gTEACHER_COL = 8;
const gPOPULAR_COL = 9;
const gTRENDING_COL = 10;
const gACTION_COL = 11;

var gStt = 1;

var gCourse = [];

var gCourseRecent = {};

var gBASE_URL_2 = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";

$(document).ready(function () {
    var vTable = $("#courses-table").DataTable({
        columns: [
            { data: gDATA[gID_COL] },
            { data: gDATA[gCOURSE_CODE_COL] },
            { data: gDATA[gCOURSE_NAME_COL] },
            { data: gDATA[gPRICE_COL] },
            { data: gDATA[gDISCOUNT_PRICE_COL] },
            { data: gDATA[gDURATION_COL] },
            { data: gDATA[gLEVEL_COL] },
            { data: gDATA[gCOVER_IMAGE_COL] },
            { data: gDATA[gTEACHER_COL] },
            { data: gDATA[gPOPULAR_COL] },
            { data: gDATA[gTRENDING_COL] },
            { data: gDATA[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gID_COL,
                render: () => {
                    return gStt++;
                }
            },
            {
                targets: gPRICE_COL,
                render: (data) => {
                    return `<span>$${data}</span>`;
                }
            },
            {
                targets: gDISCOUNT_PRICE_COL,
                render: (data) => {
                    return `<span>$${data}</span>`;
                }
            },
            {
                targets: gCOVER_IMAGE_COL,
                render: (data) => {
                    return `<img class="cover-img" alt="" src="${data}">`;
                }
            },
            {
                targets: gTEACHER_COL,
                render: (data) => {
                    for (var i in gCourse) {
                        if (gCourse[i].teacherName == data) {
                            return `<div>
                            <img class="teacher-photo" alt="" src="${gCourse[i].teacherPhoto}">
                            <span>${data}</span>
                            </div>`
                        }
                    }
                }
            },
            {
                targets: gPOPULAR_COL,
                className: "text-center",
                render: (data) => {
                    if (data == true) {
                        return `<i class="fas fa-check-circle text-success"></i>`;
                    }
                    else {
                        return `<i class="fas fa-times-circle text-danger"></i>`;
                    }
                }
            },
            {
                targets: gTRENDING_COL,
                className: "text-center",
                render: (data) => {
                    if (data == true) {
                        return `<i class="fas fa-check-circle text-success"></i>`;
                    }
                    else {
                        return `<i class="fas fa-times-circle text-danger"></i>`;
                    }
                }
            },
            {
                targets: gACTION_COL,
                defaultContent: `
                <i class="fas fa-edit text-success edit-btn" data-toggle="tooltip" data-placement="top" title="edit" style="cursor:pointer"></i>
                <i class="fas fa-trash text-danger delete-btn" data-toggle="tooltip" data-placement="top" title="delete" style="cursor:pointer"></i>`
            }
        ]
    })
    onPageLoading()
})

function onPageLoading() {
    callApiGetAllCourse();
    $("#add-new-btn").click(function () {
        onAddClick();
    })
    $("#courses-table").on("click", ".edit-btn", function () {
        onEditClick(this);
    })
    $("#courses-table").on("click", ".delete-btn", function () {
        onDeleteClick(this);
    })

}

function callApiGetAllCourse() {
    var vAPI_URL = gBASE_URL_2 + "/courses";
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        async: false,
        success: function (res) {
            console.log(res);
            gCourse = res;
            showInformation(res);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function showInformation(paramList) {
    var vTable = $("#courses-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramList);
    vTable.draw();
}

function onAddClick() {
    $("#add-new-course-modal").modal("show");
    $("#create-a-couse-btn").click(function () {
        var vCourseNew = {
            courseCode: "",
            courseName: "",
            price: 0,
            discountPrice: 0,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: "",
            isTrending: "",
        };
        readAddNewCourse(vCourseNew);
        var vDuLieuHopLe = validateAdd(vCourseNew);
        if (vDuLieuHopLe) {
            callApiToCreate(vCourseNew);
        }
    })
}

function readAddNewCourse(course) {
    course.courseCode = $("#course-code-add-input").val();
    course.courseName = $("#course-name-add-input").val();
    course.price = $("#price-add-input").val();
    course.discountPrice = $("#discount-price-add-input").val();
    course.duration = $("#duration-add-input").val();
    course.level = $("#level-add-select").val();
    course.coverImage = $("#cover-image-add-input").val();
    course.teacherName = $("#teacher-name-add-input").val();
    course.teacherPhoto = $("#teacher-image-add-input").val();
    if ($('#popular-add-input:checked').val() == "on") {
        course.isPopular = true;
    } else {
        course.isPopular = false;
    }
    if ($('#trending-add-input:checked').val() == "on") {
        course.isTrending = true;
    } else {
        course.isTrending = false;
    }
    console.log(course);
}

function validateAdd(course) {
    if (course.courseCode == "") {
        alert(`[courseCode] must be filled`);
        return false;
    }
    if (course.courseCode < 10) {
        alert(`[courseCode] must be more than 10 letters `);
        return false;
    }
    if (course.courseName == "") {
        alert(`[courseName] must be filled`);
        return false;
    }
    if (course.courseName < 20) {
        alert(`[courseName] must be more than 20 letters `);
        return false;
    }
    if (course.price == "") {
        alert(`[price] must be filled`);
        return false;
    }
    if (Number.isInteger(course.price)) {
        alert(`[price] must be integer`);
        return false;
    }
    if (course.price < 0) {
        alert(`[price] must be larger than 0`);
        return false;
    }
    if (Number.isInteger(course.discountPrice)) {
        alert(`[discountPrice] must be integer`);
        return false;
    }
    if (course.discountPrice < 0) {
        alert(`[discountPrice] must be larger than 0`);
        return false;
    }
    if (parseInt(course.discountPrice) > parseInt(course.price)) {
        alert(`[discountPrice] must be smaller than [price]`);
        return false;
    }
    if (course.duration == "") {
        alert(`[duration] must be filled`);
        return false;
    }
    if (course.level == "") {
        alert(`[level] must be selected`);
        return false;
    }
    if (course.coverImage == "") {
        alert(`[coverImage] must be filled`);
        return false;
    }
    if (course.teacherName == "") {
        alert(`[teacherName] must be filled`);
        return false;
    }
    if (course.teacherPhoto == "") {
        alert(`[teacherPhoto] must be filled`);
        return false;
    }
    return true;
}

function callApiToCreate(course) {
    const vAPI_URL = gBASE_URL_2 + "/courses";
    $.ajax({
        url: vAPI_URL,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(course),
        success: function (res) {
            console.log(res);
            alert("Successfully created");
            onPageLoading();

        },
        error: function (error) {
            console.log(error);
        }
    })
}

function onEditClick(paramElement) {
    $("#edit-course-modal").modal("show");
    var vCourse = getCourseByElement(paramElement);
    var vId = getIdByElement(paramElement);
    console.log(vId);
    showToEditModal(vCourse);
    $("#edit-a-couse-btn").click(function () {
        EditCourse(vId);
    })
}

function getCourseByElement(paramElement) {
    var vTable = $("#courses-table").DataTable();
    var vRow = $(paramElement).parents("tr");
    var vData = vTable.row(vRow).data();
    return vData;
}

function getIdByElement(paramElement) {
    var vTable = $("#courses-table").DataTable();
    var vRow = $(paramElement).parents("tr");
    var vData = vTable.row(vRow).data();
    return vData.id;
}

function callApiToShowCourse(paramId) {
    const vAPI_URL = gBASE_URL_2 + "/courses/";
    $.ajax({
        url: vAPI_URL + paramId,
        type: "GET",
        success: function (res) {
            console.log(res);

        },
        error: function (error) {
            console.log(error);
        }
    })
}

function showToEditModal(paramCourse) {
    $("#course-code-edit-input").val(paramCourse.courseCode);
    $("#course-name-edit-input").val(paramCourse.courseName);
    $("#price-edit-input").val(paramCourse.price);
    $("#discount-price-edit-input").val(paramCourse.discountPrice);
    $("#duration-edit-input").val(paramCourse.duration);
    $("#level-edit-select option:selected").text(paramCourse.level);
    $("#cover-image-edit-input").val(paramCourse.coverImage);
    $("#teacher-name-edit-input").val(paramCourse.teacherName);
    $("#teacher-image-edit-input").val(paramCourse.teacherPhoto);
    if (paramCourse.isPopular == true) {
        $("#popular-edit-input").prop("checked", true);
    } else {
        $("#popular-edit-input").prop("checked", false);
    }

    if (paramCourse.isTrending == true) {
        $("#trending-edit-input").prop("checked", true);
    } else {
        $("#trending-edit-input").prop("checked", false);
    }


}

function EditCourse(paramId) {
    var vNewUpdate = {
        id: paramId,
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: "",
    }
    readEdit(vNewUpdate);
    var vDuLieuHopLe = validateAdd(vNewUpdate);
    if (vDuLieuHopLe) {
        callApiEdit(vNewUpdate);
        console.log(vNewUpdate);
    }
}

function readEdit(newCourse) {
    newCourse.courseCode = $("#course-code-edit-input").val();
    newCourse.courseName = $("#course-name-edit-input").val();
    newCourse.price = $("#price-edit-input").val();
    newCourse.discountPrice = $("#discount-price-edit-input").val();
    newCourse.duration = $("#duration-edit-input").val();
    newCourse.level = $("#level-edit-select option:selected").val();
    newCourse.coverImage = $("#cover-image-edit-input").val();
    newCourse.teacherName = $("#teacher-name-edit-input").val();
    newCourse.teacherPhoto = $("#teacher-image-edit-input").val();
    var x = $("#popular-edit-input").is(":checked");
    var y = $("#trending-edit-input").is(":checked");
    if (x == true) {
        newCourse.isPopular = true;
    } else {
        newCourse.isPopular = false;
    }
    if (y == true) {
        newCourse.isTrending = true;
    } else {
        newCourse.isTrending = false;
    }

}

function callApiEdit(paramCourse) {
    const vAPI_URL = gBASE_URL_2 + "/courses/";
    $.ajax({
        url: vAPI_URL + paramCourse.id,
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(paramCourse),
        success: function (res) {
            console.log(res);
            alert("successfully updated");
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function onDeleteClick(paramElement) {
    var vId = getIdByElement(paramElement);
    console.log(vId);
    $("#delete-course-modal").modal("show");
    $("#delete-a-couse-btn").click(function () {
        callApiDelete(vId);
    })
}

function callApiDelete(paramId) {
    var vAPI_URL = gBASE_URL_2 + "/courses/" + paramId;
    $.ajax({
        url: vAPI_URL ,
        type: "DELETE",
        success: function (res) {
            alert(`successfully deleted`);
            onPageLoading();
        },
        error: function (error) {
            console.log(error.responseText);
        }
    })
}